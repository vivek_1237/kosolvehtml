$(document).ready(function(){
	
	$('.tab-box ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('.tab-box ul.tabs li').removeClass('current');
		$('.tab-box .tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})
	
	/*******Sidebar tab***********/	
	$('.sidebar-tab ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('.sidebar-tab ul.tabs li').removeClass('current');
		$('.sidebar-tab .tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

})